#ifndef ANOTHER_GPS_SERVER_DATA_TYPES_HPP
#define ANOTHER_GPS_SERVER_DATA_TYPES_HPP

#include <unordered_map>
#include <vector>

struct Point
{
    int transportID=0;
    int lat=0, lon=0, alt=0, speed=0, direction=0, satcnt=0;
    unsigned long time=0;
    bool signal=false;
};

struct Teltonica_Login{
    std::string imei;
};

struct Teltonica_Frame{
    unsigned long Utime=0;
    int priority=0;
    int lon=0;
    int lat=0;
    int altitude=0;
    int angle=0;
    int sattelites=0;
    int speed=0;
    int event_IO_ID=0;
    std::unordered_map<int, long> IO;
};

using Teltonica_Data = std::vector<Teltonica_Frame>;

#endif //ANOTHER_GPS_SERVER_DATA_TYPES_HPP
