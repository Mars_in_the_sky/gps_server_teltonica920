#include <vector>
#include <iostream>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <unistd.h>

#include "spdlog/spdlog.h"
#include "data_storage.hpp"
#include "PSQL_handler.hpp"

std::string DataStorage::global_emerg_path;

DataStorage::DataStorage(pqxx::connection *_read_con, pqxx::connection *_write_con, boost::mutex *_mutex)
{
    if (!_write_con || !_mutex) {
        cerr << "DataStorage::DataStorage: pointer to connection or mutex is null"<< endl << flush;
        exit(1);
    }

    write_con=_write_con;
    read_con=_read_con;
    read_mutex=_mutex;
    purging_in_progress0=false;
    purging_in_progress1=false;
}

DataStorage::~DataStorage() {
    while (purging_in_progress0) usleep(1);
    upload_points(&points_store0);
    while (purging_in_progress1) usleep(1);
    upload_points(&points_store1);
}

void DataStorage::run() {
    while (true) {
        if (points_store0.size()+points_store1.size()>1000000){
            //emergency input in file
            spdlog::set_sync_mode();
            auto daily_logger = spdlog::daily_logger_mt("emergency",global_emerg_path+"emergency_teltonica.log"
            );
            while (purging_in_progress0) usleep(1);
            purging_in_progress0=true;
            for (auto &it:points_store0){
                spdlog::get("emergency")->critical("[[{}][{}][{}][{}][{}][{}][{}][{}][{}]]",it.transportID, it.time, it.lon, it.lat, it.alt, it.direction, it.satcnt, it.speed, it.signal);
                spdlog::get("emergency")->flush();
            }
            while (purging_in_progress1) usleep(1);
            purging_in_progress1=true;
            for (auto &it:points_store1){
                spdlog::get("emergency")->critical("[[{}][{}][{}][{}][{}][{}][{}][{}][{}]]",it.transportID, it.time, it.lon, it.lat, it.alt, it.direction, it.satcnt, it.speed, it.signal);
                spdlog::get("emergency")->flush();
            }
        }


        usleep(SLEEP_DELAY);
        bool upload_res=false;

        //detect what point will use
        vector<Point>* to_upload;
        bool* lock;
        if (points_store0.size()>points_store1.size()){
            to_upload=&points_store0;
            lock=&purging_in_progress0;
        }
        else {
            to_upload=&points_store1;
            lock=&purging_in_progress1;
        }
        if (*lock) continue;
        if (to_upload->size()) {
            *lock=true;
            upload_res = upload_points(to_upload);
            if (upload_res){
                spdlog::get("logger")->info("Uploaded {} points to db.", to_upload->size());
                to_upload->clear();
            }
            else {
                spdlog::get("logger")->error("Error has occured during uploading to db.");
            }
            *lock=false;
        }
    }
}

/*!
 * \brief Store point data in RAM for further uploading to database
 * \param aTransportID   - sensor id in database
 * \param aTokenMap - map of tokens got from data package
 * \return 0 - error
 *         1 - success
 */
bool DataStorage::store(int gid, Teltonica_Data& data) {
    std::vector<Point> points_tmp;
    for (auto& it:data) {
        Point p;
        p.transportID = gid;

        p.lat = it.lat / 10;
        p.lon = it.lon / 10;
        p.alt = it.altitude;
        p.speed = it.speed;
        p.direction = it.angle;
        p.satcnt = it.sattelites;
        p.time = it.Utime / 1000;
        p.signal=false;
        if (vavidation(p)) {
            points_tmp.push_back(p);
        }
    }
    if (points_tmp.empty()) return false;
    boost::mutex::scoped_lock lock(write_mutex);
    bool b=true;
    do {
        if (!purging_in_progress0) {
            purging_in_progress0=true;
            points_store0.insert(points_store0.end(), points_tmp.begin(), points_tmp.end());
            purging_in_progress0=false;
            b=false;
        } else if (!purging_in_progress1) {
            purging_in_progress1=true;
            points_store1.insert(points_store1.end(), points_tmp.begin(), points_tmp.end());
            purging_in_progress1=false;
            b=false;
        }
        if (b) usleep(1);
    }
    while (b);
    return true;
}

bool DataStorage::upload_points(const std::vector<Point>* points) {
    boost::mutex::scoped_lock lock(write_mutex);
    work xact(*write_con);
    bool res = PSQL_handler::upload_points(*points, xact);
    if (res) {
        xact.commit();
        spdlog::get("logger")->info("Pack of points was successfully uploaded to db.");
    }
    else {
        spdlog::get("logger")->error("Error has occured during uploading to db.");
    }
    return res;
}

bool DataStorage::vavidation(Point &p) {
    if (p.satcnt<=5)
        spdlog::get("logger")->warn("Small number of satellites: {}, gid={}", p.satcnt, p.transportID);
    if ((p.time<1577836800) || (p.time>(time(nullptr)+2*86400))) {
        spdlog::get("logger")->error("Wrong time from gid={}. Time={}", p.transportID, p.time);
        return false;
    }
    return true;
}

//
//
//
