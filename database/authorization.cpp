#include "authorization.hpp"

#include <iostream>

#include <boost/thread/mutex.hpp>

#include "PSQL_handler.hpp"


Authorization::Authorization(pqxx::connection* _read_con, pqxx::connection* _write_con, boost::mutex* _mutex)
{
    if (!_read_con || !_write_con) {
        cerr << "Connection::Connection: An attempt to make a connection "
             << "with null pointer to pqxx::connection was made" << endl << flush;
        exit(1);
        // TERMINATION
    }
    read_con= _read_con;
    write_con= _write_con;
    mutex= _mutex;
    authorized=false;
    transportID = -1;
    imei="";

}

/*!
 * \brief Authorization::authorize
 * \param imei
 * \param aPassword
 * \return
 */
bool Authorization::authorize(string& imei) {
    tr_gid gid;
    string dbHash, resultHash;
    boost::mutex::scoped_lock lock(*mutex);
    work xact1(*read_con);
    gid=PSQL_handler::get_crypted_password(imei, xact1);
    //добавление записи если нету
    if (gid==-1) {
        work xact2(*write_con);
        gid = PSQL_handler::add_sensor(imei, xact2);
    }
    this->imei   = imei;
    this->transportID = gid;
    bool res = (gid != -1 && gid!= 0);
    authorized = res;
    return res;
}
