#ifndef ANOTHER_GPS_SERVER_PSQL_HANDLER_HPP
#define ANOTHER_GPS_SERVER_PSQL_HANDLER_HPP

#include <string>
#include <vector>
#include <pqxx/pqxx>
#include <data_types.hpp>

using tr_gid = int;

using namespace std;
using namespace pqxx;

struct DBConfig{
    std::string psqlHost;
    std::string psqlLogin;
    std::string psqlDbName;
    std::string psqlDbPass;
};

class PSQL_handler
{
public:
    PSQL_handler();
    ~PSQL_handler();

    static bool connectToDB(DBConfig& cnfg, pqxx::connection** _connection);

    static tr_gid get_crypted_password(string& imei, work &xact);
    static tr_gid add_sensor(string imei, work &xact);
    static int      get_transportID(string imei, work &xact);
    static bool    upload_points(const vector<Point> &points, work &xact);
};



#endif //ANOTHER_GPS_SERVER_PSQL_HANDLER_HPP
