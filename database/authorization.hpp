#ifndef ANOTHER_GPS_SERVER_AUTHORIZATION_HPP
#define ANOTHER_GPS_SERVER_AUTHORIZATION_HPP

#include <string>
#include <pqxx/pqxx>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

using namespace std;

class Authorization
{
public:
    Authorization(pqxx::connection* _read_con, pqxx::connection* _write_con, boost::mutex* _mutex);
    bool authorize(string& imei);
    bool authorized;
    int transportID;
    string imei;

private:
    pqxx::connection* read_con;
    pqxx::connection* write_con;

    boost::mutex* mutex;
};

typedef std::shared_ptr<Authorization> Auth_ptr;

#endif //ANOTHER_GPS_SERVER_AUTHORIZATION_HPP
