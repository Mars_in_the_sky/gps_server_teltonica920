#ifndef ANOTHER_GPS_SERVER_DATA_STORAGE_HPP
#define ANOTHER_GPS_SERVER_DATA_STORAGE_HPP

#define SLEEP_DELAY 7500000

#include <pqxx/pqxx>
#include <boost/thread/mutex.hpp>
#include <sstream>

#include "data_types.hpp"

using namespace std;

class DataStorage
{
public:
    DataStorage(pqxx::connection *_read_con, pqxx::connection *_write_con, boost::mutex *_mutex);
    virtual ~DataStorage();
    bool store(int aTransportID, Teltonica_Data& data);
    bool upload_points(const std::vector<Point>* points);
    void run();
    static std::string global_emerg_path;
private:
    bool vavidation(Point& p);

    pqxx::connection* write_con;
    pqxx::connection* read_con;
    boost::mutex*     read_mutex;
    boost::mutex      write_mutex;
    vector<Point>     points_store0, points_store1;
    vector<Point>     points_copy;
    bool              purging_in_progress0, purging_in_progress1;
};

#endif //ANOTHER_GPS_SERVER_DATA_STORAGE_HPP
