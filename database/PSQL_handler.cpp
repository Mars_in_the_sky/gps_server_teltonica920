#include "PSQL_handler.hpp"

#include <pqxx/pqxx>
#include <iostream>

#include "spdlog/spdlog.h"
#include "data_types.hpp"

using namespace std;
using namespace pqxx;


PSQL_handler::PSQL_handler() {
    // empty
}

PSQL_handler::~PSQL_handler() {
    // empty
}

/*!
 * \brief PSQLHandler::connectToDB
 * \param host_name
 * \param user_name
 * \param DB_name
 * \param DB_pass
 * \return
 */
bool PSQL_handler::connectToDB(DBConfig& cnfg, pqxx::connection** _connection)
{
    if (cnfg.psqlDbName.empty()) {
        cerr << "PSQLHandler::connectToDB: aDBName can not be empty" << endl;
        return false;
    }

    string con_str("dbname=" + cnfg.psqlDbName);
    if (!cnfg.psqlHost.empty()) con_str.append(" host=" + cnfg.psqlHost);
    if (!cnfg.psqlLogin.empty()) con_str.append(" user=" + cnfg.psqlLogin);
    if (!cnfg.psqlDbPass.empty())   con_str.append(" password=" + cnfg.psqlDbPass);

    try {
        *_connection = new connection(con_str);
    }
    catch (const sql_error &e) {
        cerr << e.what() << endl;
        return false;
    }
    catch (...){
        cerr << "Unknown_error "+to_string(__FILE__)+" "+to_string(__LINE__) << endl;
    }
    return true;
}

/*!
 * \brief PSQLHandler::getCryptedPassword
 * \param imei
 * \param xact
 * \return hash of user password and his id in db
 */
tr_gid PSQL_handler::get_crypted_password(string& imei, work &xact) {
    stringstream query;
    result res;
    result::const_iterator row;
    tr_gid transportID=-1;

    // first - get account id from sensors table according to anImei
    query << "SELECT globalid FROM transports WHERE gd = '" << imei << "'"
          << endl << "ORDER BY registration_time DESC"
          << endl << "LIMIT 1";

    try{
        res = xact.exec(query);
    }
    catch (const sql_error &e) {
        spdlog::get("logger")->error("{} Can't get gid form imei {}", e.what(), imei);
        return transportID;
    }
    catch (...){
        spdlog::get("logger")->error("Unknown_error  {} {}", __FILE__, __LINE__);
        return transportID;
    }

    if (res.empty()) {
        spdlog::get("logger")->error("Empty result gid form imei {}", imei);
        return transportID;
    }
    row=res.begin();
    transportID = row["globalid"].as<int>();
    spdlog::get("logger")->info("Get gid form imei {}, git={}", imei, transportID);
    spdlog::get("logger")->flush();
    return transportID;
}

tr_gid PSQL_handler::add_sensor(string imei, work &xact) {
    tr_gid transportID=-1;

    long now = time(nullptr);

    stringstream query;
    query << " INSERT INTO transports(compname, id, gd, name, last_time, lat, lon, registration_time) ";
    query << " VALUES ('logiston', 0, '" << imei << "', '', 0, 0, 0, " << now << ") ";
    query << " RETURNING ID;";

    try {
        pqxx::result res = xact.exec(query);
        transportID = res[0][0].as<int>();
        xact.commit();
        spdlog::get("logger")->info("New sensor was added to DB. imei={}; gid={}", imei, transportID);
    } catch (const sql_error &e) {
        spdlog::get("logger")->error("SQL occure error: {}. Can't add new sensor {}", e.what(), imei);
    }
    return transportID;
}


int PSQL_handler::get_transportID(string imei, work &xact) {
    result res;
    result::const_iterator row;

    stringstream query;
    query << "SELECT globalid FROM transports WHERE gd = '" << imei << "'";

    try {
        res = xact.exec(query);
    }
    catch (const sql_error &e) {
        spdlog::get("logger")->error("SQL occure error: {}. Can't get gid from {}", e.what(), imei);
    }

    row = res.begin();
    return row["globalid"].as<int>();
}


/*!
 * \brief PSQLHandler::uploadPoints
 * \param points
 * \param xact
 * \return 0
 */
bool PSQL_handler::upload_points(const vector<Point> &points, work &xact) {
    stringstream query;
    result res;
    result::const_iterator row;
    size_t i, len;
    const Point* p;
    bool return_statement=true;

    query   << "INSERT INTO points("
            << endl << "transpid, lat, lon, alt, time, speed, direction, sc, signal"
            << endl << ") VALUES"
            << endl;

    len = points.size();

    for (i = 0; i < len; i++) {
        p = &points[i];

        if (0 != i) query << ",";

        query   << "("
                << endl << "  " << p->transportID
                << endl << ", " << p->lat
                << endl << ", " << p->lon
                << endl << ", " << p->alt
                << endl << ", " << p->time
                << endl << ", " << p->speed
                << endl << ", " << p->direction
                << endl << ", " << p->satcnt
                << endl << ", " << ((p->signal) ? "TRUE" : "FALSE")
                //        << endl << ", " << now
                << endl << ")";
    }

    if (len > 0) {
        query << ";"
              << endl << "update transports set last_time=p.time, lat=p.lat, lon=p.lon, speed=p.speed, direction=p.direction "
              << endl << "from points p"
              << endl << "where transpid=globalid "
              << endl << "and p.time=(select time from points where transpid=globalid order by time desc limit 1)"
              << endl << "and p.time!=last_time;";
    }

    try {
        res = xact.exec(query);
        xact.commit();
    }
    catch (const sql_error &e) {
        spdlog::get("logger")->error("SQL occure error: {}. Can't upload points", e.what());
        return_statement=false;
    }

    return return_statement;
}
