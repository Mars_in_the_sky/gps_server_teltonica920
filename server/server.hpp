//
// Created by serhii on 06.02.20.
//

#ifndef CPP_SERVER_SERVER_HPP
#define CPP_SERVER_SERVER_HPP

#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/thread/mutex.hpp>

#include <pqxx/connection>

#include "PSQL_handler.hpp"
#include "connection.hpp"
#include "data_storage.hpp"



struct ServerConfig{
    std::string host;
    unsigned short port;
    size_t pool_size=0;
    DBConfig db_config;
};

/// Asynchronous server with threads
class Server {
public:
    Server(ServerConfig& c);

    void run_server();

    ~Server();

private:
    ServerConfig cnfg;

    unsigned long connection_number=0;

    /// The io_context object provides I/O services, such as sockets, that the server object will use.
    boost::asio::io_context io_context;
    /// The type of a TCP endpoint.
    boost::asio::ip::tcp::endpoint endpoint;
    /// Provides the ability to accept new connections (TCP).
    boost::asio::ip::tcp::acceptor acceptor;

    pqxx::connection* read_con;
    pqxx::connection* write_con;

    boost::mutex mutex;

    std::shared_ptr<DataStorage> storage;

    /// Creates a socket and initiates an asynchronous accept operation to wait for a new connection.
    void start_accept();
    /// Run the io_context object so that it will perform asynchronous operations on your behalf.
    void run();
    /// Is called when the asynchronous accept operation initiated by start_accept() finishes.
    /// It services the client request, and then calls start_accept() to initiate the next accept operation.
    void handle_accept(const std::shared_ptr<Connection>& connection,
                       const boost::system::error_code& e);
};

#endif //CPP_SERVER_SERVER_HPP