//
// Created by serhii on 06.02.20.
//

#ifndef CPP_SERVER_CLIENT_HPP
#define CPP_SERVER_CLIENT_HPP

#include <boost/asio.hpp>
#include <memory>
#include <string>

#include "data_storage.hpp"
#include "authorization.hpp"


/// Represents client connection
class Connection : public std::enable_shared_from_this<Connection> {
public:
    enum { max_length = 10240 };

    Connection(boost::asio::io_context& io_context, unsigned long& connection_number, std::shared_ptr<Authorization> auth, DataStorage* storage);

    //~Connection();

    boost::asio::ip::tcp::socket& get_socket();
    /// Read the data from the client
    void read();

private:
    boost::asio::ip::tcp::socket socket; // socket for connection
    char request[max_length];
    char response[32];

    bool end_connection=false;

    std::shared_ptr<Authorization> auth;
    DataStorage* storage;

    unsigned long N_connection;

    //void hang_up();

    size_t processing_request(size_t data_len);

    /// Send the data to the client
    void handle_read(const boost::system::error_code& error,
                     size_t bytes_transferred);
};

#endif //CPP_SERVER_CLIENT_HPP