//
// Created by serhii on 06.02.20.
//
#include <boost/bind.hpp>
#include <iostream>

#include "teltonica920.hpp"
#include "connection.hpp"
#include "spdlog/spdlog.h"

Connection::Connection(boost::asio::io_context& io_context, unsigned long& connection_number, std::shared_ptr<Authorization> auth, DataStorage* storage)
        : socket(io_context), N_connection(connection_number), auth(std::move(auth)), storage(storage) {
    spdlog::get("logger")->info("New connection. CN [{}]", connection_number++);
}

boost::asio::ip::tcp::socket& Connection::get_socket() {
    return socket;
}

void Connection::read() {
    /// read the request
    socket.async_read_some(
            boost::asio::buffer(request),
            boost::bind(&Connection::handle_read,
                        shared_from_this(),
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
}

void Connection::handle_read(const boost::system::error_code& error,
                                 size_t bytes_transferred) {
    /// send the response
    // handle the error
    if (error){
        if (error==boost::asio::error::eof) spdlog::get("logger")->info("Close connection. CN [{}]", N_connection);
        else spdlog::get("logger")->error("Close connection. CN [{}]. Error: {}", N_connection, error.message());
        spdlog::get("logger")->flush();
        return;
    }
    boost::system::error_code ec1;
    spdlog::get("logger")->info("{} Request from:{}. CN [{}]", auth->imei, socket.remote_endpoint(ec1).address().to_string(), N_connection);
    if (ec1){
        spdlog::get("logger")->error("Socket remote endpoint error: {}.", ec1.message());
    }
    else {

        size_t ans = processing_request(bytes_transferred);

        if (end_connection) {
//        hang_up();
            spdlog::get("logger")->info("Close connection. CN [{}]", N_connection);
            return;
        }
        boost::system::error_code ec2;
        spdlog::get("logger")->info("Send answer to: {}. Ans=[{},{},{},{}]. Size={}. CN [{}]",
                                    socket.remote_endpoint(ec2).address().to_string(),
                                    int(response[0]), int(response[1]), int(response[2]), int(response[3]), ans,
                                    N_connection);
        if (ec2){
            spdlog::get("logger")->error("Socket remote endpoint error: {}.", ec2.message());
        }
        else {
            socket.async_write_some(
                    boost::asio::buffer(response, ans),
                    [self = shared_from_this(), n = N_connection](const boost::system::error_code &e,
                                                                  std::size_t bytes_transferred) -> void {
                        // Read again
                        //self->read();
                        if (e) {
                            spdlog::get("logger")->error("Async write error: {}. CN[{}].", e.message(), n);
                        } else {
                            self->read();
                        }
                    });
        }
    }
}

//void Connection::hang_up() {
//    boost::system::error_code ignored_ec;
//    socket.shutdown(boost::asio::socket_base::shutdown_both, ignored_ec);
//    socket.close();
//    if (ignored_ec){
//        spdlog::get("logger")->error("Error with connection. CN [{}]. {}", N_connection, ignored_ec.message());
//    }
//}
//
//Connection::~Connection() {
//    hang_up();
//    spdlog::get("logger")->info("Close connection. CN [{}]", N_connection);
//}

size_t Connection::processing_request(size_t data_len) {
    bool res;
    size_t ans_size=0;
    std::unique_ptr<Teltonica920> request_handler(new Teltonica920);
    Teltonica920_Request_Type RT=request_handler->parse(request, data_len, auth->imei, N_connection);
    if (RT==Teltonica920_Request_Type::Login){
        auto log=request_handler->get_login();
        spdlog::get("logger")->info("RT: Login. imei={}. CN [{}]", log.imei, N_connection);
        res=auth->authorize(log.imei);
        if (res) {
            request_handler->get_answer(response, ans_size, res);
            spdlog::get("logger")->info("Auth is success. Gid={}. CN [{}]", auth->transportID, N_connection);
        }
        else {
            spdlog::get("logger")->error("Auth isn't success. CN [{}]", N_connection);
        }
        end_connection=!res;
    }
    else if (RT==Teltonica920_Request_Type::Data && auth->authorized){
        auto data=request_handler->get_data();
        res=storage->store(auth->transportID, data);
        if (!res){
            spdlog::get("logger")->error("Error in request! CN [{}]", N_connection);
        }
        else {
            request_handler->get_answer(response, ans_size, res);
        }
        end_connection = !res;
    }
    else {//else Teltonica920_Request_Type::Error
        spdlog::get("logger")->error("Wrong request! CN [{}]", N_connection);
        end_connection = true;
    }
    return ans_size;
}
