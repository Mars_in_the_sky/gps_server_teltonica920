//
// Created by serhii on 06.02.20.
//

#include "server.hpp"
#include "connection.hpp"
#include "spdlog/spdlog.h"

#include <boost/bind.hpp>
#include <iostream>
#include <functional>
#include <memory>
#include <thread>
#include <vector>
#include <boost/thread.hpp>

enum TCPConsts {
    max_connections = 10000,
    max_threads = 16,
};

Server::Server(ServerConfig& c)
        : cnfg(c),
          endpoint(boost::asio::ip::tcp::v4(), c.port),
        // initialises an acceptor to listen on TCP port port
          acceptor(io_context, endpoint, /* reuse_addr = */ true){

    bool res=PSQL_handler::connectToDB(c.db_config, &read_con) && PSQL_handler::connectToDB(c.db_config, &write_con);
    if (!res) {
        cerr << "ERROR:Server::Server: could not open a connection to db"<< endl << flush;
        exit(1);
    }
    storage.reset(new DataStorage(read_con, write_con, &mutex));
}

void Server::run_server() {
    acceptor.listen(max_connections);

    start_accept();

    boost::thread storageLoop(
            boost::bind(
                    &DataStorage::run
                    , boost::ref(storage)
            )
    );

    if (cnfg.pool_size==0) cnfg.pool_size=max_threads;
    std::cout << "Run server on " << cnfg.port << " port with "
              << max_connections << " max connections and "
              << cnfg.pool_size << " max threads." << std::endl;

    // running io_contexts in threads?
    std::vector<std::thread> threads;
    for (int i = 0; i < cnfg.pool_size; i++)
        threads.emplace_back(std::thread(std::bind(&Server::run, this)));
    for (auto &thread : threads)
        thread.join();
}

void Server::handle_accept(const std::shared_ptr<Connection>& connection,
                               const boost::system::error_code& error) {
    if (!error) {
        connection->read();
    }
    if (error){
        spdlog::get("logger")->error("srvr handl_accept. error: {}", error.message());
        if (error==boost::asio::error::no_descriptors){
            spdlog::get("logger")->flush();
            usleep(SLEEP_DELAY*2);
            exit(1);
        }
    }

    start_accept();
}

void Server::start_accept() {
    std::shared_ptr<Connection> connection
            (new Connection(acceptor.get_executor().context(), connection_number, std::make_shared<Authorization>(read_con, write_con, &mutex), storage.get()));
    acceptor.async_accept(connection->get_socket(),
                          boost::bind(&Server::handle_accept,
                                      this, connection,
                                      boost::asio::placeholders::error));
}

void Server::run() {
    io_context.run();
}

Server::~Server() {
    if (read_con) {
        read_con->disconnect();
        delete read_con;
    }
    if (write_con) {
        write_con->disconnect();
        delete write_con;
    }

    storage.reset();
}
