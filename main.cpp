#include <iostream>

#include <boost/program_options.hpp>

#include "server/server.hpp"
#include "spdlog/spdlog.h"
#include "utils/init_logger.hpp"
#include "utils.hpp"

namespace po = boost::program_options;

int main() {
    std::string s1, s2;
    ServerConfig config;
    po::variables_map config_map;
    po::options_description desc ("options");

    desc.add_options()
            ("host", po::value<std::string>(&config.host), "host")
            ("port", po::value<unsigned short>(&config.port), "port")
            ("pool_size", po::value<size_t>(&config.pool_size), "pool_size")
            ("psqlHost", po::value<std::string>(&config.db_config.psqlHost), "psqlHost")
            ("psqlLogin", po::value<std::string>(&config.db_config.psqlLogin), "psqlLogin")
            ("psqlDbName", po::value<std::string>(&config.db_config.psqlDbName), "psqlDbName")
            ("psqlDbPass", po::value<std::string>(&config.db_config.psqlDbPass), "psqlDbPass")
            ("log_path", po::value<std::string>(&s1), "log_path")
            ("log_data_path", po::value<std::string>(&s2), "log_data_path");

    read_config(desc, config_map);

    init_logger(s2, s1);

    DataStorage::global_emerg_path=s1;

    spdlog::get("logger")->info("Starting up the machine on port {}", config.port);
    spdlog::get("logger")->flush();
    Server s(config);
    s.run_server();
    spdlog::get("logger")->info("End of working");
    return 0;
}
