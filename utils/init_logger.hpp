#ifndef ANOTHER_GPS_SERVER_INIT_LOGGER_HPP
#define ANOTHER_GPS_SERVER_INIT_LOGGER_HPP

#include "spdlog/spdlog.h"

void init_logger(const std::string& path_to_async_file_logger, const std::string& path_to_sync_file_logger){
    spdlog::set_pattern("[%H:%M:%S %f] [%l] %v");
    //initialization of logger for only data
    size_t q_size = 4096;
    spdlog::set_async_mode(q_size);
    auto async_file = spdlog::daily_logger_st(
            "async_logger",
            path_to_async_file_logger+"data_teltonica.log"
            );
    //
    //initialization of logger
    spdlog::set_sync_mode();
    spdlog::flush_on(spdlog::level::info);
    auto daily_logger = spdlog::daily_logger_mt("logger",
            path_to_sync_file_logger+"teltonica.log"
            );
    //
}

#endif //ANOTHER_GPS_SERVER_INIT_LOGGER_HPP
