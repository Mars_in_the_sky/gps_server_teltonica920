#ifndef ANOTHER_GPS_SERVER_UTILS_HPP
#define ANOTHER_GPS_SERVER_UTILS_HPP

#include <fstream>
#include <iostream>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

inline void read_config(po::options_description& aDesc, po::variables_map& aMap){
    std::ifstream file("config.ini");

    if (!file.is_open()) {
        std::cout<<"can't open file:config.ini";
    }

    po::store(po::parse_config_file(file, aDesc, true), aMap);
    file.close();
    po::notify(aMap);
}

#endif //ANOTHER_GPS_SERVER_UTILS_HPP
