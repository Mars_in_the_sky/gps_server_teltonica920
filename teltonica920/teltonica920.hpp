#ifndef ANOTHER_GPS_SERVER_TELTONICA920_HPP
#define ANOTHER_GPS_SERVER_TELTONICA920_HPP

#include "data_types.hpp"

#include <cstddef>
#include <utility>
#include <cstdint>

enum class Teltonica920_Request_Type{
    Error,
    Login,
    Data
};

class Teltonica920{
private:
    class Byte_by_Byte{
    public:
        Byte_by_Byte(const char* data, size_t len);
        unsigned long get_next(int bytes);
        bool get_next(int bytes, unsigned long& res) noexcept;
    private:
        const char *data;
        size_t len;
        size_t cur;
    };
    Teltonica920_Request_Type RT;
    Teltonica_Login Login;
    Teltonica_Data Data;
    void IO_N_byte_parse(int N_byte, int size_byte, Byte_by_Byte& zip, std::unordered_map<int, long>& IO);
    void save_request(const char* data, size_t len, std::string& imei);
    // is_good=false;
public:
    //bool good() const;
    Teltonica920_Request_Type parse(const char* data, size_t len, std::string& imei,unsigned long N_connection);
    Teltonica_Login get_login();
    Teltonica_Data get_data();
    void get_answer(char* resp, size_t& size, bool res);
};

#endif //ANOTHER_GPS_SERVER_TELTONICA920_HPP
