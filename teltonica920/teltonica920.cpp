//
// Created by serhii on 11.02.20.
//

#include <string>
#include <exception>

#include "spdlog/spdlog.h"
#include "CRC-16.hpp"
#include "teltonica920.hpp"


Teltonica920::Byte_by_Byte::Byte_by_Byte(const char *data, size_t len):data(data), len(len), cur(0){}

unsigned long Teltonica920::Byte_by_Byte::get_next(int bytes) {
    if (cur+bytes>len) throw std::length_error("Error in Byte_by_Byte");
    unsigned long res=0;
    unsigned long p=1;
    for (int i=bytes-1;i>=0;i--){
        if (long(data[i])<0){
            res+=(256+long(data[i]))*p;
        }
        else res+=long(data[i])*p;
        p*=256;
    }
    data+=bytes;
    cur+=bytes;
    return res;
}

bool Teltonica920::Byte_by_Byte::get_next(int bytes, unsigned long& res) noexcept {
    if (cur+bytes>len) return false;
    res=0;
    unsigned long p=1;
    for (int i=bytes-1;i>=0;i--){
        if (long(data[i])<0){
            res+=(256+long(data[i]))*p;
        }
        else res+=long(data[i])*p;
        p*=256;
    }
    data+=bytes;
    cur+=bytes;
    return true;
}

void Teltonica920::IO_N_byte_parse(int N_byte, int size_byte, Teltonica920::Byte_by_Byte &zip,
                                                 std::unordered_map<int, long> &IO) {
    for (int j=0; j<N_byte;j++){
        IO.insert(std::pair<int, long>(zip.get_next(1), zip.get_next(size_byte)));
    }

}

void Teltonica920::save_request(const char *data, size_t len, std::string& imei) {
    int n__;
    int m__;
    std::string msg, orig_msg;
    for (size_t i=0; i<len;i++){
        orig_msg+=data[i];
        if (int(data[i])<0){
            n__=(256+int(data[i]))%16;
            m__=(256+int(data[i]))/16;
        }
        else{
            n__=int(data[i])%16;
            m__=int(data[i])/16;
        }
        if (m__<10) msg+=char(m__+'0');
        else msg+=char(m__+'a'-10);
        if (n__<10) msg+=char(n__+'0');
        else msg+=char(n__+'a'-10);
    }
    spdlog::get("async_logger")->info("[[{}][{}]]", imei, msg);
    spdlog::get("async_logger")->info("[|[{}]||[{}]|]", imei, orig_msg);
}

Teltonica920_Request_Type Teltonica920::parse(const char *data, size_t len, std::string& imei, unsigned long N_connection) {
    save_request(data, len, imei);
    Byte_by_Byte zip(data, len);
    RT=Teltonica920_Request_Type::Error;
    try{
        long first_two=zip.get_next(2);
        if (first_two==0x0000){
            //data
            if (zip.get_next(2)!=0x0000) throw std::logic_error("4 zeros");
            if (zip.get_next(4)!=len-4*3) throw std::logic_error("data field length");

            //check crc-16
            Byte_by_Byte crc_zip(data+(len-4), 4);
            if (crc_16((unsigned char*)(data+8), len-12)!=crc_zip.get_next(4)) throw std::logic_error("CRC-16: NOT OK.");

            //codec
            if (zip.get_next(1)!=0x08) throw std::logic_error("Codec ID");

            //data frames
            unsigned long amnt_of_frames=zip.get_next(1);
            for (unsigned long i=0;i<amnt_of_frames;i++){
                Teltonica_Frame frame;
                frame.Utime=zip.get_next(8);
                frame.priority=zip.get_next(1);
                frame.lon=zip.get_next(4);
                frame.lat=zip.get_next(4);
                frame.altitude=zip.get_next(2);
                frame.angle=zip.get_next(2);
                frame.sattelites=zip.get_next(1);
                frame.speed=zip.get_next(2);
                frame.event_IO_ID=zip.get_next(1);
                int N_total_IO=zip.get_next(1);
                int N1, N2, N4, N8;

                N1=zip.get_next(1);
                IO_N_byte_parse(N1, 1, zip, frame.IO);

                N2=zip.get_next(1);
                IO_N_byte_parse(N2, 2, zip, frame.IO);

                N4=zip.get_next(1);
                IO_N_byte_parse(N4, 4, zip, frame.IO);

                N8=zip.get_next(1);
                IO_N_byte_parse(N8, 8, zip, frame.IO);

                Data.push_back(std::move(frame));
            }

            if (zip.get_next(1)!=amnt_of_frames) throw std::logic_error("Number of Data 1 != Number of Data 2");

            RT=Teltonica920_Request_Type::Data;
        }

        else if (first_two==0x000f){
            //login
            unsigned long n__;
            while (zip.get_next(1,n__)) Login.imei+=char(n__);
            RT=Teltonica920_Request_Type::Login;
        }
    }
    catch (std::length_error& ex){
        RT=Teltonica920_Request_Type::Error;
        spdlog::get("logger")->warn("Error in parsing: {}. imei={}. CN [{}]", ex.what(), imei, N_connection);
    }
    catch (std::logic_error& ex){
        RT=Teltonica920_Request_Type::Error;
        spdlog::get("logger")->warn("Error in parsing: {}. imei={}. CN [{}]", ex.what(), imei, N_connection);
    }
    std::string log_str;
    for (auto&it:Data){
        log_str+="([time:"+std::to_string(it.Utime)+
                "][pr:"+std::to_string(it.priority)+
                "][lon:"+std::to_string(it.lon)+
                "][lat:"+std::to_string(it.lat)+
                "][alt:"+std::to_string(it.altitude)+
                "][ang:"+std::to_string(it.angle)+
                "][stt:"+std::to_string(it.sattelites)+
                "][spd:"+std::to_string(it.speed)+"])";
    }
    spdlog::get("logger")->info("DATA:{}. imei={}. CN [{}]", log_str, imei, N_connection);
    return RT;
}

Teltonica_Login Teltonica920::get_login() {
    return Login;
}

Teltonica_Data Teltonica920::get_data() {
    return Data;
}

void Teltonica920::get_answer(char *resp, size_t &size, bool res) {
    if (RT==Teltonica920_Request_Type::Login){
        resp[0]=(res)?1:0;
        size=1;
    }
    else if (RT==Teltonica920_Request_Type::Data){
        size=4;
        resp[0]=0;
        resp[1]=0;
        resp[2]=0;
        resp[3]=(res)?Data.size():0;
    }
    else {
        resp[0]=0;
        size=1;
    }
}